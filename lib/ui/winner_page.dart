import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';

class WinnerPage extends StatefulWidget {
  final String items;

  const WinnerPage({Key? key, required this.items}) : super(key: key);

  @override
  _WinnerPageState createState() {
    return _WinnerPageState();
  }
}

class _WinnerPageState extends State<WinnerPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        backgroundColor: Colors.yellow[100],
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Lottie.asset(
              'assets/jsons/trophy.json',
            ),
            Text(
              widget.items,
              style: const TextStyle(fontSize: 50, color: Colors.grey),
            ),
            Container(
              margin: const EdgeInsets.only(top: 16),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  "Re Spin..!",
                  style: GoogleFonts.anton(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
