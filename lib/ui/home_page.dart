import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:what_ever_eat/ui/winner_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  List<String> listFood = [];
  final textEditingController = TextEditingController();

  late final SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    _initPref();
  }

  _initPref() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      listFood = prefs.getStringList("LIST_FOOD") ?? [];
    });
  }

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        FocusScope.of(context).requestFocus(FocusNode());
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "WhatEver Eat",
            style: GoogleFonts.anton(),
          ),
        ),
        bottomNavigationBar: Container(
          margin: const EdgeInsets.all(16),
          child: ElevatedButton(
            onPressed: () {
              if (listFood.length > 1) {
                _showDialog();
                Timer(const Duration(seconds: 4), () {
                  Navigator.pop(context);
                  _randomNumber();
                });
              } else {
                _showSnackBar("please add more than 1 item");
              }
            },
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
            ),
            child: Text(
              "Spin...!",
              style: GoogleFonts.anton(
                fontSize: 20,
                color: Colors.white,
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: TextField(
                        maxLines: 1,
                        controller: textEditingController,
                        decoration: const InputDecoration(border: OutlineInputBorder(), labelText: "Your Food"),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ElevatedButton(
                        child: Text(
                          "ADD+",
                          style: GoogleFonts.anton(
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () {
                          if (textEditingController.text.trim().isNotEmpty) {
                            _addItem();
                          }
                        },
                      ),
                    )
                  ],
                ),
                ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: listFood.length,
                  itemBuilder: (BuildContext context, int index) {
                    var items = listFood[index];
                    return _listItem(items, index);
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _listItem(String items, int index) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        color: Colors.brown[100],
        borderRadius: BorderRadius.circular(8),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Expanded(
              child: ListTile(
                title: Text(items),
              ),
            ),
            GestureDetector(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(16),
                ),
                width: 32,
                height: 32,
                child: const Icon(
                  Icons.delete_forever,
                  color: Colors.white,
                ),
              ),
              onTap: () {
                _removeItem(items, index);
              },
            ),
          ],
        ),
      ),
    );
  }

  _addItem() async {
    FocusScope.of(context).requestFocus(FocusNode());
    _showSnackBar("added item \"${textEditingController.text}\"");

    setState(() {
      listFood.add(textEditingController.text);
    });
    textEditingController.clear();
    await prefs.setStringList('LIST_FOOD', listFood);
  }

  _removeItem(String items, int index) async {
    _showSnackBar("removed item \"$items\"");
    setState(() {
      listFood.removeAt(index);
    });
    await prefs.setStringList('LIST_FOOD', listFood);
  }

  _randomNumber() async {
    Random random = Random();
    int randomNumber;
    randomNumber = random.nextInt(listFood.length);
    await _goNextPage(randomNumber);
  }

  _goNextPage(int randomItem) {
    //Go Next Page
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WinnerPage(
          items: listFood[randomItem],
        ),
      ),
    );
  }

  _showDialog() {
    showDialog<String>(
      context: context,
      barrierDismissible: false,
      barrierColor: Colors.white,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () {
            return Future.value(false);
          },
          child: AlertDialog(
            backgroundColor: Colors.white,
            elevation: 0,
            content: Lottie.asset(
              'assets/jsons/food_think.json',
            ),
          ),
        );
      },
    );
  }

  _showSnackBar(String message) {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
